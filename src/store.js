import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isAuth: false,
        version: "3.1.4",
        validationColor: "#00802b",
        bingoColor: "grey"
    },
    mutations: {
        addBingo(state, leBingo) {
            if (localStorage.getItem("xariBingo") === null || localStorage.getItem("xariBingo") === undefined || localStorage.getItem("xariBingo") === "") {
                let lesBingos = [];
                lesBingos.push(leBingo);
                localStorage.setItem("xariBingo", JSON.stringify(lesBingos));

                alert("Bingo Ajouté !");
            } else {
                let lesBingos = JSON.parse(localStorage.getItem("xariBingo"));

                // Recherche si le bingo existe déjà
                let indexBingo = null;
                lesBingos.forEach(function (element, index) {
                    if (element[0] === leBingo[0]) {
                        indexBingo = index
                    }
                });

                if (indexBingo != null) {
                    lesBingos[indexBingo] = leBingo
                } else {
                    lesBingos.push(leBingo);
                }

                localStorage.setItem("xariBingo", JSON.stringify(lesBingos));

                alert("Bingo Ajouté !");
            }
        },
        deleteBingo(state, leBingo) {
            let lesBingos = JSON.parse(localStorage.getItem("xariBingo"));

            let indexToDelete = lesBingos.indexOf(leBingo[0]);
            lesBingos.splice(indexToDelete, 1);


            localStorage.setItem("xariBingo", JSON.stringify(lesBingos));
            alert("Bingo supprimé !");
        },
    },
    actions: {}
})
