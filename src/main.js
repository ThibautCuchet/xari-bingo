import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import {faDice, faSave, faTimes, faCheck, faColumns, faTextWidth, faEraser, faShareAlt, faPlus, faTrashAlt, faCrosshairs, faEdit} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'
import firebase from "firebase/app"
import 'firebase/auth'
import 'firebase/database'

require('animate.css');

Vue.use(BootstrapVue);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);
Vue.config.productionTip = false;

var firebaseConfig = {
  apiKey: "AIzaSyAFStRx4uB2f1EOawcWUcDxhI3mfa0_AG0",
  authDomain: "xari-bingo.firebaseapp.com",
  databaseURL: "https://xari-bingo.firebaseio.com",
  projectId: "xari-bingo",
  storageBucket: "",
  messagingSenderId: "239031198371",
  appId: "1:239031198371:web:eccd6f01e3fab4444ad516"
};

firebase.initializeApp(firebaseConfig);

library.add(faDice);
library.add(faSave);
library.add(faCheck);
library.add(faTimes);
library.add(faColumns);
library.add(faTextWidth);
library.add(faEraser);
library.add(faShareAlt);
library.add(faPlus);
library.add(faTrashAlt);
library.add(faCrosshairs);
library.add(faEdit);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
