import Vue from 'vue'
import Router from 'vue-router'
import Bingo from "./components/Bingo";
import BingoPart from "./components/BingoPart";

Vue.use(Router);

export default new Router({
    base: "/xari-bingo",
    routes: [
        {
            path: '*',
            redirect: {name: 'bingo'}
        },
        {
            path: '/',
            redirect: {name: 'bingo'}
        },
        {
            path: '/bingo',
            name: 'bingo',
            component: Bingo,
        },
        {
            path: '/bingo/public',
            name: 'bingo-part',
            component: BingoPart
        },
    ],
})
